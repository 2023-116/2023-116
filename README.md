# Depresio

This Git repository contains the source code and documentation for an AI-Powered Virtual Stress Management Assistant for IT Professionals - A Comprehensive Solution for Achieving Work-Life Balance and Reducing Stress for IT Professionals. This system provides a tailored and innovative solution to support IT professionals in their quest for work-life balance and stress reduction. By leveraging cutting-edge technology, we aim to create an accessible and user-friendly platform that offers personalized recommendations, productivity-enhancing tools, and opportunities for social engagement, all designed to address the unique challenges faced by IT professionals in their daily lives, using natural language processing (NLP) and machine learning (ML) techniques.

The system includes various modules such as emotion recognition and analysis, personalized therapy recommendations, chatbot-based counseling, personalized mood tracking, cognitive-behavioral therapy, stress management and etc.

The codebase is written in Python and uses popular ML and NLP libraries such as TensorFlow, Keras, Scikit-learn, NLTK, and SpaCy. The repository also includes pre-trained models, datasets, and scripts for training and testing the models. For the frontend is written by React JS.

The documentation includes detailed instructions on how to set up and run the system, as well as explanations of the underlying algorithms and methodologies used in the AI models. Additionally, the repository includes a list of potential future improvements and directions for research.

## **Main Objective of this Project**

Through this research, we hope to contribute to the growing body of knowledge surrounding work-life balance and stress management, particularly within the IT sector. Our goal is to develop a practical, impactful, and scalable solution that empowers IT professionals to take control of their well-being and achieve a healthier, more satisfying work-life balance.

## **Main Research Question**

The research problem we aim to address is the growing need for an effective solution to help IT professionals manage their stress and achieve a healthy work-life balance. Given the unique challenges faced by individuals working in the IT sector, it is crucial to develop a comprehensive and personalized approach that caters to their specific needs and preferences.

## **Individual Research Question**

_IT20210892_

-  Can we develop a personalized virtual voice assistant that accurately detects and reduces stress levels of Sri Lankan IT professionals, and can it be integrated into their work routine to promote a healthier work-life balance?

_IT20211578_

-  Can machine learning algorithms accurately identify stress levels based on vocal emotion in Sri Lankan IT professionals?

_IT20128722_

-  How to recommend correct personalized treatment plan for Sri Lankan IT professionals?

_IT20211264_

-  Can a personalized music therapy playlist system be developed to reduce stress levels in Sri Lankan IT professionals?

## **Individual Research Objectives**

_IT20210892_

-  Design and develop the AI assistant's conversational capabilities, with a focus on understanding and responding to Sri Lankan English accent.

-  Implement a continuous learning mechanism that adapts to user feedback to improve the AI assistant's language understanding.

-  Integrate personalized work-break reminders and leisure activity suggestions based on the user's work patterns, preferences, and mental state.

-  Create a secure and confidential environment to ensure user privacy and data protection

_IT20211578_

-  Develop a multi-modal emotion detection system that combines linguistic, vocal, and visual cues for accurate stress level assessment.

-  Incorporate context-awareness to differentiate between work-related and personal-life stressors.

-  Develop a real-time stress monitoring system that provides immediate feedback to users.

-  Refine emotion detection algorithms using machine learning techniques for improved accuracy and reliability.

-  Validate the emotion detection system through user testing and feedback.

_IT20128722_

-  Implement a reinforcement learning algorithm that continually refines the treatment plan based on user feedback and progress.

-  Implement a reinforcement learning algorithm that continually refines the treatment plan based on user feedback and progress.

-  Design a gamified progress-tracking system that encourages user engagement and adherence to the treatment plan.

-  Develop a recommendation engine to suggest personalized activities and interventions based on user preferences and needs.

-  Conduct user testing and evaluations to measure the effectiveness of personalized treatment plans.

_IT20211264_

-  Utilize advanced music information retrieval techniques and machine learning algorithms to analyze and select music tracks matching user preferences and therapeutic properties.

-  Develop a dynamic playlist generation system that adapts to the user's real-time emotional state.

-  Develop a dynamic playlist generation system that adapts to the user's real-time emotional state.

-  Create a user-friendly interface for browsing and listening to personalized music therapy playlists.

-  Evaluate the impact of personalized music therapy on stress reduction through user testing and feedback.
