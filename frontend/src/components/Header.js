import React from 'react';
import '../styles/header.scss';
import logo from '../images/Depresio_Logo.jpeg';

const Header = () => {
   return (
      <header className="header--line">
         <div className="container header pt--24 pb--16">
            <div className="header__left">
               <a href="/" className="logo-wrapper">
                  <img src={logo} alt="Header Logo" className="header-logo" />
               </a>
               <ul>
                  <li>
                     <a href="#">Product</a>
                     <a href="#">Solutions</a>
                     <a href="#">Open Source</a>
                     <a href="#">Pricing</a>
                  </li>
               </ul>
            </div>
            <div className="header__right">
               <button>Sign In</button>
               <button>Sign Up</button>
            </div>
         </div>
      </header>
   );
};

export default Header;
