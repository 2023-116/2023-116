import React from 'react';
import Header from '../components/Header';

const MusicTherapy = () => {
   return (
      <>
         <Header />
         <div className="container">Music Therapy</div>
      </>
   );
};

export default MusicTherapy;
