import React from 'react';
import Header from '../components/Header';

const TreatmentPlan = () => {
   return (
      <>
         <Header />
         <div className="container">Treatment Plan</div>
      </>
   );
};

export default TreatmentPlan;
