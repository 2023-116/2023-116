import './styles/Base.scss';
import { createBrowserRouter, createRoutesFromElements, RouterProvider, Route, Link } from 'react-router-dom';
import Home from './Home';
import MusicTherapy from './pages/MusicTherapy';
import TreatmentPlan from './pages/TreatmentPlan';

const router = createBrowserRouter(
   createRoutesFromElements(
      <Route path="/">
         <Route index element={<Home />} />
         <Route path="/music-therapy" element={<MusicTherapy />} />
         <Route path="/treatment-plan" element={<TreatmentPlan />} />
      </Route>
   )
);

function App() {
   return (
      <>
         <RouterProvider router={router} />
      </>
   );
}

export default App;
